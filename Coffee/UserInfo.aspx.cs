﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Coffee
{
    public partial class UserInfo : System.Web.UI.Page
    {
        public string UserID = "11111";          //用户账号
        public string UserName;        //用户昵称
        public string UserImg;        //用户头像图片
        public string UserPassworld;   //密码
        public string UserPlace;       //所在地 
        public string UserSex;         //性别
        public string UserTel;         //手机号
        public Data dt = new Data();

        protected void Page_Load(object sender, EventArgs e)
        {
            initData();
            lab_id.Text = UserID;
            tbx_userplace.Text = UserPlace;
        }

        /*初始化用户数据*/
        public void initData()
        {
            UserName = dt.user_name(UserID);    //初始化昵称
            UserPlace = dt.user_place(UserID);  //初始化城市
            UserSex = dt.user_sex(UserID);      //初始化性别
            UserTel = dt.user_tel(UserID);      //初始化电话
            //UserImg = dt.user_img(UserID);      //初始化头像
        }
    }
}