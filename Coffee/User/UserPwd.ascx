﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserPwd.ascx.cs" Inherits="Coffee.User.UserPwd" %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>unique</title>
<link type="text/css" href="css.css" rel="stylesheet" />
<script type="text/javascript" src="js/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/js.js"></script>

</head>

<body>
 <div class="vipBox">
  <div class="vipLeft">
   <h2 class="headImg"><img src="images/vipImg.jpg" width="183" height="169" /></h2>
   <h3 class="vipName">测试webqin</h3>
   <dl class="vipNav">
    <dt class="vip_1 vipCur">买家中心</dt>
     <dd><a href="UserOrder.ascx">我的订单</a></dd>
    <dt class="vip_2">账户设置</dt>
     <dd><a href="UserInfo.ascx">个人信息</a></dd>
     <dd class="ddCur"><a href="#">密码修改</a></dd>
   </dl><!--vipNav/-->
  </div><!--vipLeft/-->
  <div class="vipRight">
   <h2 class="vipTitle">密码修改</h2>
   
   <form action="#" class="registerform">
      <table class="grzx" width="705" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="90">新密码：</td>
          <td width="430"><input type="text" class="text inputxt" name="userpassword" datatype="*6-16" nullmsg="请设置密码！" errormsg="密码范围在6~16位之间！" /></td>
          <td rowspan="4" valign="top"><div id="tx"><img src="images/vipImg.jpg" /></div></td>
        </tr>
        <tr>
          <td>确认密码：</td>
          <td><input type="text" class="text inputxt"  name="userpassword2" datatype="*" recheck="userpassword" nullmsg="请再输入一次密码！" errormsg="您两次输入的账号密码不一致！"  /></td>
        </tr>
        <tr>
          <td>验证码：</td>
          <td><input name="" type="text" class="text2" /></td>
        </tr>
        <tr>
          <td><input name="" value="马上修改" type="submit" class="submit" /></td>
          <td></td>
        </tr>
      </table>
      </form>
  </div><!--vipRight/-->
  <div class="clears"></div>
 </div><!--vipBox/-->
</body>