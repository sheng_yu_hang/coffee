﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Coffee.User
{
    public class Data
    {
        CoffeeDataContext db = new CoffeeDataContext();

        /*获取用户昵称*/
        public string user_name(string userid)
        {
            var results = from r in db.Users
                          where r.UserID == userid  //这个userid要换成赵博玉给的用户ID的变量名
                          select r.UserName;
            return results.ToArray<String>()[0];
        }

        /*获取用户所在地*/
        public string user_place(string userid)
        {
            var results = from r in db.Users
                          where r.UserID == userid  //这个userid要换成赵博玉给的用户ID的变量名
                          select r.UserPlace;
            return results.ToArray<String>()[0];
        }

        /*获取用户性别*/
        public string user_sex(string userid)
        {
            var result = from r in db.Users where r.UserID == userid select r.UserSex;

            return result.ToArray<string>()[0];

        }

        /*获取电话号码*/
        public string user_tel(string userid)
        {
            var results = from r in db.Users
                          where r.UserID == userid  //这个userid要换成赵博玉给的用户ID的变量名
                          select r.UserTel;
            return results.ToArray<String>()[0];
        }

        ///*获取用户头像图片地址*/
        //public string user_img(string userid)
        //{
        //    var results = from r in db.Users
        //                  where r.UserID == userid  //这个userid要换成赵博玉给的用户ID的变量名
        //                  select r.UserImg;         //数据库User表要添加一个UserImg字段
        //    return results.ToArray<String>()[0];
        //}
    }
}