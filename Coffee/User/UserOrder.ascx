﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserOrder.ascx.cs" Inherits="Coffee.User.UserOrder" %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>unique</title>
<link type="text/css" href="css.css" rel="stylesheet" />
<script type="text/javascript" src="js/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/js.js"></script>

</head>

<body>
 <div class="vipBox">
  <div class="vipLeft">
   <h2 class="headImg"><img src="images/vipImg.jpg" width="183" height="169" /></h2>
   <h3 class="vipName">测试webqin</h3>
   <dl class="vipNav">
    <dt class="vip_1 vipCur">买家中心</dt>
     <dd class="ddCur"><a href="#">我的订单</a></dd>
    <dt class="vip_2">账户设置</dt>
     <dd><a href="UserInfo.ascx">个人信息</a></dd>
     <dd><a href="UserPwd.ascx">密码修改</a></dd>
   </dl><!--vipNav/-->
  </div><!--vipLeft/-->
  <div class="vipRight">
   <h2 class="vipTitle">我的订单</h2>
   
   <h2 class="oredrName">
    我的订单 <span style="margin-left:40px;">待发货 <span class="red">10</span> </span>
    <span>待收货 <span class="red">15</span></span>
    </h2>
    <table class="vipOrder">
     <tr>
      <td><a href="proinfo.html"><img src="images/phone.png" width="60" height="55"></a></td>
      <td>张益达</td>
      <td>￥16.9<br />支付宝支付</td>
      <td>2014年6月23日11:32:17</td>
      <td><a href="success.html"><strong>等待付款</strong></a></td>
      <td><a href="vipXiaofei.html">查看</a></td>
     </tr>
     <tr>
      <td><a href="proinfo.html"><img src="images/phone.png" width="60" height="55"></a></td>
      <td>张益达</td>
      <td>￥16.9<br />支付宝支付</td>
      <td>2014年6月23日11:32:17</td>
      <td><a href="success.html"><strong>等待付款</strong></a></td>
      <td><a href="vipXiaofei.html">查看</a></td>
     </tr>
     <tr>
      <td><a href="proinfo.html"><img src="images/phone.png" width="60" height="55"></a></td>
      <td>张益达</td>
      <td>￥16.9<br />支付宝支付</td>
      <td>2014年6月23日11:32:17</td>
      <td><a href="success.html"><strong>等待付款</strong></a></td>
      <td><a href="vipXiaofei.html">查看</a></td>
     </tr>
     <tr>
      <td><a href="proinfo.html"><img src="images/phone.png" width="60" height="55"></a></td>
      <td>张益达</td>
      <td>￥16.9<br />支付宝支付</td>
      <td>2014年6月23日11:32:17</td>
      <td><a href="success.html"><strong>等待付款</strong></a></td>
      <td><a href="vipXiaofei.html">查看</a></td>
     </tr>
     <tr>
      <td><a href="proinfo.html"><img src="images/phone.png" width="60" height="55"></a></td>
      <td>张益达</td>
      <td>￥16.9<br />支付宝支付</td>
      <td>2014年6月23日11:32:17</td>
      <td><a href="success.html"><strong>等待付款</strong></a></td>
      <td><a href="vipXiaofei.html">查看</a></td>
     </tr>
    </table><!--vipOrder/-->
  </div><!--vipRight/-->
  <div class="clears"></div>
 </div><!--vipBox/-->
</body>